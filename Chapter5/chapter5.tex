%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Third Chapter **********************************
%*******************************************************************************
\chapter{Human Cancer Classification using Rna-Seq Data}
\label{chap:chp5}
% **************************** Define Graphics Path **************************
\ifpdf
    \graphicspath{{Chapter5/Figs/Raster/}{Chapter5/Figs/PDF/}{Chapter5/Figs/}}
\else
    \graphicspath{{Chapter5/Figs/Vector/}{Chapter5/Figs/}}
\fi

\section{Background on Cancer Classification}



%Talk about early diagnosis of cancer
As reported by the American Cancer Society, cancer is the second top leading cause of death in the United Stated, after the heart diseases. In 2017, more than $1.600.000$ people are expected to be diagnosed with cancer and $600.000$ of those who (this value could be interpreted as around $1.600$ people per day) are expected to lose their lives to cancer \cite{smith2017cancer}. 
Early diagnosis of cancer could increase the chances of survival and could enhance the prognostication process.
Moreover, determining the type of cancer has severe importance in following a relevant treatment. Accordingly, there are numerous studies employing machine learning techniques to predict cancer. \bigbreak Most of the previous works on cancer prediction are using microarray technologies and the majority of those works are addressing the binary cancer prediction problem. In other words, they address only one type of cancer. Readers are referred to \cite{Basford2012,Reena2011,Mohamed2016} for a comprehensive study of cancer prediction using microarrays. As explained in Chapter \ref{chap:chp4}, RNA-Seq technologies provide more stable and reliable measurements than the microarray technologies. Unlike many preceding studies, this work addresses the cancer prediction problem using data coming from TCGA cancer database. Furthermore, we are solving a multi-class cancer prediction problem consisting of 14 different types of cancer selected according to the leading estimated death rates by cancer type in 2017 statistic provided by the American Cancer Society illustrated in Figure \ref{fig:cancer_estimates}. 


\begin{figure} [H]
	%\centering
	\includegraphics[width=1\textwidth]{cancer_estimates}
	\caption{Estimated cancer cases and deaths of 2017}
	\label{fig:cancer_estimates}
\end{figure}

\bigbreak
For the reasons above, this work addresses an up-to-date problem and the scope of this work is wider than the aforementioned studies. 


\section{Methodology}


Figure \ref{fig:experiment_pipeline} illustrates the experiment pipeline step by step. The TCGA data is retrieved through GMQL web interface with the following query below. The further steps of the experiments are performed using PyGMQL.

\begin{verbatim}
DATA_SET = SELECT() HG19_TCGA_rnaseqv2_gene;
MATERIALIZE DATA_SET INTO TCGA_Data;
\end{verbatim}




\begin{figure} [H]
	%\centering
	\includegraphics[width=1\textwidth]{experiment_design}
	\caption{Pipeline of the experiment}
	\label{fig:experiment_pipeline}
\end{figure}


\subsection{Preprocessing of TCGA Data}

	The original TCGA data consist of 31 different types of cancer. There are $9.825$ samples with $20.271$ diverse genes. The TCGA cancer type names and codes of the 14 chosen cancers are provided in Table \ref{table:tcga_14} for the reproducibility of the results \cite{TCGA_33}. After selecting the 14 cancers, the sample size shrinks to $5.271$. This operation is followed by the filtering of the genes containing missing values in more than $\%40$ of the samples and the number of genes is reduced to $17.282$. Subsequently, the missing values of the \textit{sample-gene matrix} are imputed using the lowest expression value of the gene among all of the samples. Later on, the data normalization is performed by transforming the distribution of every gene into unit variance in order to remove the biases described in Chapter \ref{chap:chp4}. The code snippet using PyGMQL for loading and preprocessing of the data is given below. The interested readers are referred to the GitHub repository \cite{PyGMQL_Repo} for the PyGMQL source code and documentation.
	
	\begin{python}
import gmql as gl
path = './Datasets/tcga_data/files/'
# the normalized count value is selected
selected_values = ['normalized_count']
# we are only interested in the gene symbol
selected_region_data = ['gene_symbol']
# all metadata are selected
selected_meta_data = []
gs = gl.ml.GenometricSpace()
# to load the data
gs.load(path,selected_region_data,selected_meta_data,
selected_values,full_load=False)
# matrix representation
gs.to_matrix(selected_values, selected_region_data, default_value=None)
# compact representation of region and metadata
gs.set_meta(['biospecimen_sample__sample_type_id',
'manually_curated__tumor_tag','biospecimen_sample__sample_type'])

from gmql.ml.algorithms.preprocessing import Preprocessing
# pruning the genes that contain more than %40 missing values
gs.data = Preprocessing.prune_by_missing_percent(gs.data, 0.4)
# missing value imputation
gs.data = Preprocessing.impute_using_statistics(gs.data, method='min')
# gene standardization
gs.data = Preprocessing.to_unit_variance(gs.data)
	\end{python}


	

	
	\begin{table} [h]
		\caption{TCGA names and abbreviations of the chosen cancer types}
		\label{table:tcga_14}
			\begin{tabular}{l@{\hskip 1in}c@{\hskip 0.5in}c}
				\toprule
				Cancer Types & Abbreviation \\
				\midrule
				Acute Myeloid Leukemia & LAML \\
				Prostate adenocarcinoma & PRAD \\
				Bladder Urothelial Carcinoma  & BLCA \\
				Breast invasive carcinoma & BRCA \\
				Colon adenocarcinoma & COAD \\
				Glioblastoma multiforme & GBM \\
				Liver hepatocellular carcinoma & LIHC \\
				Lung adenocarcinoma & LUAD \\
				Lung squamous cell carcinoma & LUSC \\
				Lymphoid Neoplasm Diffuse Large B-cell Lymphoma & DLBC \\
				Ovarian serous cystadenocarcinoma & OV \\
				Pancreatic adenocarcinoma & PAAD \\
				Rectum adenocarcinoma & READ \\
				Uterine Corpus Endometrial Carcinoma & UCEC \\
				\bottomrule[0.5pt]
			\end{tabular}
	\end{table}
	
	%Acs_cancers_by_mortality = ['laml','prad','BLCA','BRCA','COAD','GBM','LIHC','LUAD','LUSC','DLBC','OV','PAAD','READ','UCEC']

\subsection{Gene Selection}

All of the experiments employ Chi-squared ($\chi^2$) feature selection technique for selecting the top $2.000$ informative genes. Figure \ref{fig:correlogram} depicts the impact of gene selection on our experiment dataset by comparing the sample correlograms computed using different number of genes. The correlograms are computed using \textit{Pearson} correlation coefficient. The sample-sample \textit{pearson} correlation matrix is sorted by the cancer types prior to the rendering of the figures. The \texttt{G} parameter in the figures stands for the number of genes. Yellow color represents the maximum correlation, while blue color denotes no correlation. The correlogram \ref{fig:a} uses all of the genes. Therefore, more noise is present in the first correlogram than the second correlogram that uses 2.000 genes selected using the Chi-squared feature selection. In fact, the second correlogram is the clearest one among the four correlograms. One can even distinguish the cancer types by observing the squares on the main diagonal. However, choosing too many genes causes loss of information as seen in  \ref{fig:c} and more obviously, in \ref{fig:d}.

\begin{figure*} [h]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{corr_all}
		\caption{G=17282}
		\label{fig:a}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{corr_2000}
		\caption{G=2000}
		\label{fig:b}
	\end{subfigure}
	
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{corr_200}
		\caption{G=200}
		\label{fig:c}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{corr_50}
		\caption{G=50}
		\label{fig:d}
	\end{subfigure}
	\caption{Comparison of sample correlation matrices}\label{fig:correlogram}
\end{figure*}


\subsection{Cancer Prediction}

The stratified 10-fold cross validation is used in all of the experiments. Regarding the multi-class classification, one-versus-rest (OvR) strategy is chosen for the logistic regression and SVM experiments. The experiments are conducted using PyGMQL and Scikit-Learn. The random seed number is set to 123 in an effort to provide reproducible results.

\subsubsection{SVM}

Four kernels of support vector machines (sigmoid, polynomial, linear, radial basis function) are used in the experiments. As a result, the linear kernel SVM with \textit{l2} regularization outperformed the other kernels. This result is not surprising since some other previous works in the gene expression classification also concluded that the linear kernel yields better results than the other kernels \cite{DeviArockiaVanitha2014, furey2000support}. Table \ref{table:svm_linear} and Figure \ref{fig:svm_linear} denote the classification performance of the SVM linear kernel classifier with \textit{l2} penalization.

\begin{figure} [h]
	%\centering
	\includegraphics[width=1\textwidth]{svm-linear-l2}
	\caption{Confusion matrix for SVM linear kernel classifier}
	\label{fig:svm_linear}
\end{figure}
\begin{table} [h]
	\caption{The results of the SVM linear kernel classifier}
	\label{table:svm_linear}
	\begin{center}
			\begin{tabular}{lrrrr}
				\toprule
				{} &  precision &  recall &  fscore &  support \\
				\midrule
				prad &       0.92 &    0.93 &    0.93 &      427 \\
				read &       1.00 &    0.99 &    0.99 &     1218 \\
				paad &       0.79 &    0.79 &    0.79 &      329 \\
				gbm  &       0.95 &    0.81 &    0.88 &       48 \\
				lihc &       0.99 &    0.93 &    0.96 &      174 \\
				coad &       1.00 &    0.96 &    0.98 &      173 \\
				brca &       0.99 &    0.98 &    0.99 &      424 \\
				laml &       0.89 &    0.87 &    0.88 &      576 \\
				ucec &       0.84 &    0.87 &    0.85 &      554 \\
				lusc &       1.00 &    0.99 &    0.99 &      309 \\
				blca &       0.94 &    0.95 &    0.94 &      183 \\
				luad &       1.00 &    1.00 &    1.00 &      550 \\
				ov   &       0.36 &    0.41 &    0.38 &      105 \\
				dlbc &       0.96 &    0.98 &    0.97 &      201 \\
				\bottomrule
			\end{tabular}
	\end{center}
\end{table}


\subsubsection{Logistic Regression}

The very first technique applied in the experiments is the Logistic Regression which is applied to the cancer prediction problem under two different parametrization. One with the \textit{l1} penalization and the other with \textit{l2} penalization. The outcome of model with \textit{l2} penalization is slightly better. In fact the outcome of SVM linear and logistic regression are quite similar. \citet{james2013introduction} indicated that the similarity between the two classifiers is due to the similarity between their loss functions. They also pointed out that SVMs perform better if the classes are well-separated. Logistic regression, on the other hand, yields better results if overlapping exists among the classes. Figure \ref{fig:logistic_l1} illustrates the confusion table and Table \ref{table:logistic_l1} represents the performance of the logistic regression classifier with \textit{l1} penalization.

\begin{figure} [h]
	%\centering
	\includegraphics[width=1\textwidth]{logistic_l1}
	\caption{Confusion matrix for Logistic Regression with \textit{l1} penalization}
	\label{fig:logistic_l1}
\end{figure}
\begin{table} [h]
	\caption{The results of the Logistic Regression with \textit{l1} penalization classifier}
	\label{table:logistic_l1}
	\begin{center}
		\scalebox{1}{
		\begin{tabular}{lrrrr}
			\toprule
			{} &  precision &  recall &  fscore &  support \\
			\midrule
			prad &       0.93 &    0.93 &    0.93 &      427 \\
			read &       0.99 &    0.99 &    0.99 &     1218 \\
			paad &       0.76 &    0.80 &    0.78 &      329 \\
			gbm  &       0.98 &    0.83 &    0.90 &       48 \\
			lihc &       1.00 &    0.91 &    0.95 &      174 \\
			coad &       1.00 &    0.97 &    0.99 &      173 \\
			brca &       0.99 &    0.99 &    0.99 &      424 \\
			laml &       0.89 &    0.87 &    0.88 &      576 \\
			ucec &       0.83 &    0.88 &    0.85 &      554 \\
			lusc &       1.00 &    1.00 &    1.00 &      309 \\
			blca &       0.91 &    0.95 &    0.93 &      183 \\
			luad &       1.00 &    1.00 &    1.00 &      550 \\
			ov   &       0.35 &    0.33 &    0.34 &      105 \\
			dlbc &       0.96 &    0.94 &    0.95 &      201 \\
			\bottomrule
		\end{tabular}
	}
	\end{center}
\end{table}


\subsubsection{Random Forests}

Random forest is a suitable algorithm for analyzing the gene expression dataset since it is able to handle a vast amount of variables. In our experiments, \textit{gini} index is employed to define the quality of a decision tree split. The experiments are repeated under a different number of estimators, i.e., trees. In this experiment, random forests with 10, 100 and 200 trees are used. Typically the number of estimators are chosen to be approximately around to the square root of the number of features. We observed that the performance of the algorithm is stabilized after a certain number of estimators. Both 100 and 200 estimators yield similar results. 10 estimators instead, yield a slightly worse result. Figure \ref{fig:rf-200} depicts the confusion matrix of the classification results of random forests with 200 estimators and Table \ref{table:rf-200} shows the precision, recall and f-score metrics for each cancer type.
 
\begin{figure} [h]
	%\centering
	\includegraphics[width=1\textwidth]{rf-200}
	\caption{Random forests with 200 estimators}
	\label{fig:rf-200}
\end{figure}

\begin{table} [h]
	\caption{The results of the random forest classifier with 200 estimators}
	\label{table:rf-200}
	\begin{center}
		\begin{tabular}{lrrrr}
			\toprule
			{} &  precision &  recall &  fscore &  support \\
			\midrule
			prad &       0.92 &    0.93 &    0.92 &      427 \\
			read &       0.97 &    0.99 &    0.98 &     1218 \\
			paad &       0.73 &    0.88 &    0.80 &      329 \\
			gbm  &       0.92 &    1.00 &    0.96 &       48 \\
			lihc &       0.99 &    0.97 &    0.98 &      174 \\
			coad &       1.00 &    1.00 &    1.00 &      173 \\
			brca &       0.99 &    0.99 &    0.99 &      424 \\
			laml &       0.89 &    0.89 &    0.89 &      576 \\
			ucec &       0.87 &    0.85 &    0.86 &      554 \\
			lusc &       1.00 &    0.98 &    0.99 &      309 \\
			blca &       0.95 &    0.91 &    0.93 &      183 \\
			luad &       1.00 &    0.99 &    0.99 &      550 \\
			ov   &       0.00 &    0.00 &    0.00 &      105 \\
			dlbc &       0.95 &    0.96 &    0.95 &      201 \\
			\bottomrule
		\end{tabular}
	\end{center}
\end{table}



\section{Discussion and Conclusion}

Table \ref{table:accuracy_comparison} compares the overall accuracies of the employed classifiers evaluated using 10-fold cross validation. The highest accuracy is achieved using logistic regression with \textit{l2} penalization. This is reasonable since the gene expression datasets often hold substantial multicollinearity. Random forests with 100 and 200 estimators also yielded high scores. Yet, they were also the fastest to compute. As for the support vector classifiers, the best performance is achieved with the linear kernel. Overall, accuracy greater than 0.9 in a multi-class cancer classification problem is noteworthy. By observing the confusion matrices and the classification results, one can easily notice that the f-score of the Ovarian serous cystadenocarcinoma (ov) is the lowest in all of the top-performing classifiers (it is zero in random forests classifier and below). Ovarian serous cystadenocarcinoma is often misclassified as Pancreatic adenocarcinoma (paad) and vice versa. One cause of this problem could be that the genes that are informative in detecting the Ovarian serous cystadenocarcinoma and/or Pancreatic adenocarcinoma cancer are not considered important in presence of the other genes that identify other cancer types with more sample values. We also suspected that while we are removing the genes containing missing values in more than $\%40$ of the samples, we might be removing some genes that distinguish certain types of cancer. In order to validate our hypothesis, we repeated the experiments without filtering out the genes containing more missing values than a threshold. The results did not produce a significant change in the f-score of the 'ov' and 'paad' cancers. 

	\begin{table} 
		\caption{Overall comparison of the classifiers}
		\label{table:accuracy_comparison}
		\begin{tabular}{l@{\hskip 1in}c@{\hskip 0.5in}c}
			\toprule
			Method & 10-CV mean accuracy\\
			\midrule
			svm-rbf & 0.9070 \\
			svm-polynomial & 0.8664 \\
			svm-sigmoid  & 0.7753 \\
			svm-linear-\textit{l2} & 0.9296 \\
			random forests 10 estimators & 0.9070 \\
			random forests 100 estimators & 0.9261 \\
			random forests 200 estimators & 0.9277 \\
			logistic regression with \textit{l1} penalization & 0.9275 \\
			logistic regression with \textit{l2} penalization & 0.9358 \\
			\bottomrule[0.5pt]
		\end{tabular}
	\end{table}




