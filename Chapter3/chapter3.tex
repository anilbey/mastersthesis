%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Third Chapter **********************************
%*******************************************************************************
\chapter{System Architecture for the Analysis of GenoMetric Space Data}
\label{chap:chp3}
%Efficient Clustering and Classification of Next Generation Sequencing Data
%Machine Learning Models for the Analysis of the NGS Data
%Classification and Cluster Analysis of RNA-Sequencing Data
%Gene expression clustering & classification
%Classification of RNA-Seq Data
%On the classification and clustering of gene expression data
%Gene Classification and CLustering
%Gene Expression Data Classification
%Analysis of Machine Learning Techniques for Gene Selection and Classification of Rna-Seq Data



% **************************** Define Graphics Path **************************

\ifpdf
    \graphicspath{{Chapter3/Figs/Raster/}{Chapter3/Figs/PDF/}{Chapter3/Figs/}}
\else
    \graphicspath{{Chapter3/Figs/Vector/}{Chapter3/Figs/}}
\fi

This section introduces the GenoMetric Space module of the PyGMQL Python library. 
GenoMetric Space module leverages the power of PyGMQL, by adding a wide range of the machine learning and data analysis packages. GenoMetric Space module also provides efficient data structures for both parsing and the in-memory processing of GDM data. PyGMQL is publicly available on the Python Package Index and it is ready to be used by the bio-informatics practitioners \footnote{PyGMQL is available at \protect\url{https://pypi.python.org/pypi/gmql}}.

\section{Loading the Materialized Data into Memory}
%Talk about the memory problems you faced and your solutions to them.
%TODO Arif's feedback: 2 ways of parsing the materialized data
%Talk to Luca about it
Materialized results of a GMQL dataset often belong to the scale of gigabytes. Therefore, it is a demanding task to process the materialized GMQL output in memory. 

To enhance the performance in both reading and processing of the large amount of data, GenoMetric Space module provides the following optimizations for parsing the data efficiently.

The first optimization is to parse only the region data and metadata of interest. Generally it is the case that only a subset of region data and metadata are required for the tertiary analysis. 
For example, in a binary tumor classification setting, \footnote{In binary tumor classification, the task is to generalize the samples into two categories implying whether the sample is tumorous or not.} we are only interested in a sample identifier, a gene region identifier and the expression value of that region for the corresponding sample as the region attributes. As for the metadata attributes, we only need to use the metadata attribute indicating whether the sample is tumorous or not; this metadata attribute is going to be used in the training of the model and in the estimation of the model performance. Hence the remaining metadata and region data attributes are unrelated. To this extent, GenoMetric Space module comprises functionality to parse only a subset of the region data and metadata.

	\begin{table}[h]
		\caption{Time comparison of the parsing methods}
		
		\label{table:load_optimization_performance}
		\begin{center}
			
			
			\begin{tabular}{l|c}
				
				Method & Parsing time (min.) \\
				\midrule
				Normal & 17 \\
				Omit zeros & 11 \\
				
				
			\end{tabular}
		\end{center}
	\end{table}


A further optimization method is called \textit{omit zeros} method. This method omits the parsing of the gene expression values that are equal to zero. In other words, the zero values are treated as missing values. Those missing values later can be set to zero again or they can be imputed using statistical methods. Chapter \ref{chap:chp4} discusses the missing value imputation techniques. \texttt{Omit zeros} technique significantly improves the parsing process. The table \ref{table:load_optimization_performance} illustrates the runtime comparison of the \texttt{omit zeros} parsing method and the normal parsing method. Note that, the experiments are conducted on the same machine using the TCGA-KIRC dataset with the same region data and metadata attributes.
The query below retrieves the Kidney Renal Clear Cell Carcinoma (KIRC) tumor data from The Cancer Genome Atlas (TCGA). 
\begin{verbatim}
DS = SELECT(manually_curated__tumor_tag == "kirc") HG19_TCGA_rnaseqv2_gene;
MATERIALIZE DS INTO Tcga_Kirc;
\end{verbatim}


 As the table shows, \texttt{omit zeros} optimization takes less time to read the region data into the memory. As the sparsity of the dataset grows, \texttt{omit zeros} method performs better.




\section{Region Data Representation}


GenoMetric Space module employs advanced hierarchical indexing structures to operate on complex structures of genomic data. Hierarchical / Multi-level indexing is a fascinating technique as it allows certain sophisticated data analysis and manipulation, particularly when the data is high dimensional \cite{AdvancedIndex}. 


\begin{figure}
	%\centering
	\includegraphics[width=1\textwidth]{hierarchical_region}
	\caption{Hierarchical indexed representation of region data}
	\label{fig:hier_region}
\end{figure}


Figure \ref{fig:hier_region} shows how the GDM region data could be represented using the hierarchical index. The region attributes to form the hierarchical index are adjustable. \texttt{Chr, left, right, strand} and \texttt{gene\_symbol } are chosen for this illustration. Instead, the columns are indexed by the sample identifier. 

%Then talk about the design of a sophisticated hierarchical index for genomic data processing
%Refer to pandas cookbook examples, arithmetic (such as normalization(mean removal, )), sliding, sorting(for identifying the most expressed genes for example), grouping, apply, visualizing ... by using TCGA data of course

%Region data, multi-indexed region data, multi-valued multi indexed region data, multi-ref region data

\subsection{Operations on the Region Data}

The multi-level indexing representation enables effective processing of the region data in a  manner that follows the standard conventions of the Python community. The use of hierarchical index gives users the freedom to explore the data either by using a single index or by using a customized combination of indices. Figure \ref{fig:cross_sec} for instance, depicts how cross-section could be performed to retrieve the regions of \textit{'chromosome8'} located on the \textit{'+'} strand. Figure \ref{fig:level_vals} illustrates a more complex operation by using boolean operators to filter the rows i.e. all of the resulting rows in the figure have their left position bigger than or equal to $600.000$.


\begin{figure}[h]
	%\centering
	\includegraphics[width=1\textwidth]{cross_section_2}
	\caption{Cross section operation to filter the region data}
	\label{fig:cross_sec}
\end{figure}

\begin{figure}[h]
	%\centering
	\includegraphics[width=1\textwidth]{get_level_vals}
	\caption{Filtering using a boolean mask}
	\label{fig:level_vals}
\end{figure}



\section{Compact Structure}
%find a good name for it and stick to it (compact structure for example)
The metadata of the samples are kept in a separated dataframe having the same index (\texttt{sample id}) as the region dataframe. However, another feature of the GenoMetric Space module is to form a \textit{compact structure} by constructing a two-sided hierarchical indexing structure of both region data and the metadata combined together. This structure allows the data to be filtered, sliced, diced, sorted, grouped and pivoted by using both the region data and metadata simultaneously. Figure \ref{fig:compact_repr} demonstrates the \textit{compact structure} on an excerpt of the TCGA-KIRC dataset. GenoMetric Space also provides the flexibility to modify the metadata that are represented inside the index without having to reload the data. Since the metadata dataframe is kept separately, any other metadata can be inserted into or deleted from the compact structure at any time. For example, the gender of the patient can later be replaced with the age of the patient or the daily drug dose usage of the patient or any other metadata determined by the user based on the case of study. Later, those metadata attributes can be used by the machine learning algorithms.

\begin{figure}[h]
	%\centering
	\includegraphics[width=1\textwidth]{compact_view}
	\caption{Compact representation}
	\label{fig:compact_repr}
\end{figure}



\section{Support for Multi-Ref Mapped Data}
The \texttt{MAP} operation of GMQL is able to perform the mapping of the samples to more than one reference. In case of mapping with \textit{N} references, the number of resulting samples are equal to \textit{N} times the number of the input samples, as already defined in Section \ref{subsec:domain-specific-gmql}. Consequently, the output data to be processed grows dramatically with the number of references to be mapped. Accordingly, GenoMetric Space provides a particular loading function for the data mapped with multiple references. While loading the data, GenoMetric Space asks for an extra parameter to represent the unique identifier metadata attribute to separate the data by the number of references. The data is now loaded into a list of GenoMetric Space data structure. Length of that list is equal to the number of references used inside the \texttt{MAP} operation. This feature allows the references to be analyzed separately. Further, GenoMetric Space implements a \texttt{merge} function to merge both the region data and the metadata of different references into one, should the need arise. This merge function takes a parameter denoting the unique identifier metadata attribute to identify the identical samples having different references, in order to merge them into one single data structure.

\section{Text Analytics Using Metadata}
%Change the topic & make bag of words a subtopic
%NLP on METADATA
%Text processing of METADATA
% Information Retrieval on METADATA
% Talk about all of the NLP you did.

This section describes how the text mining and information retrieval approaches can be employed to model the metadata. As already told in Chapter \ref{chap:chp2} many publicly available experiment datasets (such as TCGA or ENCODE) provide the metadata alongside with their processed data. Thus, there is an immense potential of information that could be extracted from metadata. For this purpose, PyGMQL applies various information retrieval and text mining techniques on metadata. The main intention of this section is to build metadata-based models that are capable of summarizing and describing a set of samples. 
%TF-IDF
In information retrieval, \textit{tf–idf}, short for term frequency–inverse document frequency, is a numerical statistic that is intended to reflect how important a word is to a document in a collection or corpus \cite{rajaraman_ullman_2011}. \textit{Tf} stands for term frequency and accordingly; the terms having the highest \textit{tf} value are the most frequent terms occurring inside the document. However, this metric is not practical since the most frequent terms are not informative regarding the contents of a single document. Hence, it is also important to know the rare words that can help distinguish a document among the others.  To overcome this problem, \textit{idf} (inverse document frequency) is taken into account. \textit{Idf} instead, measures whether a term is rare or common over all of the documents. As shown in \ref{eq:idf}, \textit{Idf} is computed by taking the logarithm of the division of the number of documents over the number of documents that contain the term.

\begin{equation}
	\label{eq:idf}
	idf_i = log( \dfrac{N}{d\textit{fi}})
\end{equation}

\begin{align*}
\text{where:}\quad 
N &= \text{Number of documents}\\
df_i &= \text{Number of documents containing term \textit{i}}
\end{align*}
%provide the formula of idf
\textit{Tf-idf} is computed as the multiplication of \textit{tf} and \textit{idf}, equation \ref{eq:tf-idf}. Term frequency (\textit{tf}) considers all of the terms as equally important, however \textit{tf-idf}, weights the terms by their uniqueness to the document.

\begin{equation}
	\label{eq:tf-idf}
	tfidf_i,_j = tf_i,_j * idf_i
\end{equation}



\begin{align*}
	\text{where:}\quad 
	tf_i,_j &= \text{Term frequency of term i in document j}\\
	idf_i &= \text{Inverse document frequency of term \textit{i}}
\end{align*}

 \textit{Tf-idf} is considered one of the most common text-weighting techniques. Today, more than 80\% of the digital library recommendation systems use \textit{tf-idf} \cite{Beel2016}.
 
 PyGMQL, processes the metadata prior to the tf-idf computations. First of all, the tokenization process applies. Given a sequence of characters, tokenization is the operation of cutting the sequence into parts, called \textit{tokens}. After the tokenization, stop words removal takes place. Stop word removal is the operation of removing the most common words in a language. PyGMQL removes the stop words in the English language such as "that", "this", "the", "who" etc. In addition to this, stop words in the genomic domain such as "biospecimen" and "tcga" are also filtered out since they are not informative. Moreover, the metadata attributes containing Uniform Resource Locator (URL) and Universally Unique Identifier (UUID) are eliminated. Finally, the tf-idf values are computed for each term in the document. As a result, PyGMQL yields the best descriptive metadata for any given set of samples. 
 Another feature of PyGMQL is to draw the cloud of words visual representation across a collection of samples. Cloud of words, also known as Tag Cloud, is a method of visualizing the free format text \cite{Tag_Cloud}. Figure \ref{fig:cloud_of_words} illustrates an example of how the results of a clustering algorithm can be visually interpreted by using the tf-idf and the cloud of words visualization facilities of PyGMQL. Refer to Chapter \ref{chap:chp4} for the explanation of the clustering module of PyGMQL.
 
 
\begin{figure}
	%\centering
	\includegraphics[width=1\textwidth]{cloud_of_words}
	\caption{Cloud of words representation}
	\label{fig:cloud_of_words}
\end{figure}


