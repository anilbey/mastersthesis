%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Second Chapter *********************************
%*******************************************************************************

\chapter{Summary of Data Extraction Method}
\label{chap:chp2}
\ifpdf
    \graphicspath{{Chapter2/Figs/Raster/}{Chapter2/Figs/PDF/}{Chapter2/Figs/}}
\else
    \graphicspath{{Chapter2/Figs/Vector/}{Chapter2/Figs/}}
\fi


\section{Genomic Data Model (GDM)}


GDM is a data model that acts as a general schema for genomic repositories. The GDM datasets are literally collections of samples, where each sample consists of two parts, the \textit{region data}, which describe portions of the DNA, and the \textit{metadata}, which describe the sample specific properties\cite{Ceri2016}. Each GDM dataset is associated with a data schema in which the first five attributes are fixed in order to represent the region coordinates and the sample identifier. The fixed region attributes consist of the chromosome which the region belongs to, left and right ends within the chromosome and the value denoting the DNA strand that contains the region. \footnote{DNA consists of two strands which are read in opposite directions by the biomolecular mechanism of the cell.} Besides the fixed region attributes there can be other attributes associated with the DNA region. The metadata are represented with format-free attribute-value pairs, storing the information about the sample. Figure \ref{fig:gdm_region} provides an excerpt of GDM region data. As seen, the first five columns represent are the fixed region attributes and the last column, in this case, is denoting the p-value of the region significance. Figure \ref{fig:gdm_meta}, instead represents the sample-specific metadata attributes. 
It is to be observed that the first columns of both figure \ref{fig:gdm_region} and \ref{fig:gdm_meta} are the sample id, which provides a mapping between the region and the metadata of the same sample.



\begin{figure}[h]
    \centering
    \includegraphics[width=1\textwidth]{GDM_region}
    \caption{An excerpt of region data}
    \label{fig:gdm_region}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=1\textwidth]{GDM_meta}
    \caption{An excerpt of metadata}
    \label{fig:gdm_meta}
\end{figure}
 


\clearpage

%\tochide
\section{GenoMetric Query Language (GMQL)}
The GenoMetric Query Language (GMQL) is a high-level query language designed for large-scale genomic data management. The name is derived from its ability to deal with genomic distances. GMQL is capable of supporting queries over thousands of heterogeneous genomic datasets and it is adequate for efficient big data processing.

GMQL extends conventional algebraic operations with bioinformatics domain-specific operations specifically designed for genomics; thus, it supports knowledge discovery across thousands or even millions of samples, both for what concerns regions that satisfy biological conditions and their relationship to experimental, biological or clinical metadata \cite{Masseroli2015}. GMQL's innate ability to manipulate metadata is highly valuable since many publicly available experiment datasets (such as TCGA or ENCODE) provide the metadata alongside with their processed data. 
GMQL operations form a closed algebra: results are
expressed as new datasets derived from their operands. Thus, operations typically have a region-based part and a metadata part; the former one builds new regions, the latter one traces the provenance of each resulting sample.
A GMQL query (or program) is expressed as a sequence of GMQL operations, each with the following structure:
\begin{verbatim}
<variable> = operation(<parameters>) <variables>
\end{verbatim}
where each variable stands for a GDM dataset.
Operators apply to one or more operand variables and construct one result variable; parameters are specific for each operator. 
Most GMQL operations can be seen as extensions of the relational algebra operations tailored to the needs of genomics. These operations are called the \textit{relational} operations. Aside from the \textit{relational} operations, GMQL supports \textit{domain-specific} operations as well. 

\subsection{Relational GMQL Operations}

\begin{itemize}
	\item \texttt{SELECT} operator applies on metadata and selects the input samples that satisfy the specified metadata predicates. The region data and the metadata of the resulting samples are kept unaltered.

	\item \texttt{ORDER} operator orders samples, regions or both of them; the order is ascending as default and can be turned to descending by an explicit indication. Sorted samples or regions have a new attribute order, added to the metadata, regions or both of them; the value of \texttt{ORDER} reflects the result of the sorting.
	
	\item \texttt{PROJECT} operator applies on regions and keeps the input region attributes expressed in the result as parameters. It can also be used to build new region attributes as scalar expressions of region attributes (e g., the \texttt{length} of a region as the difference between its \texttt{right} and \texttt{left} ends). Metadata are kept unchanged.
	

	\item \texttt{EXTEND} operator generates new metadata attributes as a result of aggregate functions applied to the region attributes. The supported aggregate functions are \texttt{COUNT} (with no argument), \texttt{BAG} (applicable to attributes of any type) and \texttt{SUM, AVG, MIN, MAX, MEDIAN, STD} (applicable to attributes of numeric types).
	
	\item \texttt{GROUP} operator is used for grouping both regions and metadata according to distinct values of the grouping attributes. For what concerns metadata, each distinct value of the grouping attributes is associated with an output sample, with a new identifier explicitly created for that sample; samples having missing values for any of the grouping attributes are discarded. The metadata of output samples, each corresponding a to given group, are constructed as the union of metadata of all the samples contributing to that group; consequently, metadata include the attributes storing the grouping values, that are common to each sample in the group.
	
	\item \texttt{MERGE} operator merges all the samples of a dataset into a single sample, having all the input regions as regions and the union of the sets of input attribute-value pairs of the dataset samples as metadata.
	
	\item \texttt{UNION} operator applies to two datasets and builds their union, so that each sample of each operand contributes exactly to one sample of the result; if datasets have different schemas, the result schema is the union of the two sets of attributes of the operand schemas, and in each resulting sample the values of the attributes missing in the original operand of the sample are set to null. Metadata of each sample are kept unchanged.
	
	\item \texttt{DIFFERENCE} operator applies to two datasets and preserves the regions of the first dataset which do not intersect with any region of the second dataset; only the metadata of the first dataset are maintained.
	
	
\end{itemize}

\subsection{Domain-specific GMQL Operations}
\label{subsec:domain-specific-gmql}
We next focus on domain-specific operations, which are more specifically responding to genomic management requirements: the unary operation \texttt{COVER} and the binary operations \texttt{MAP} and \texttt{JOIN}.

\begin{itemize}
	
	\item \texttt{COVER} operation is widely used in order to select regions which are present in a given number of samples; this processing is typically used in the presence of overlapping regions, or of replicate samples belonging to the same experiment. The grouping option allows grouping samples with similar experimental conditions and produces a single sample for each group. For what concerns variants:
	\begin{itemize}
		\item \texttt{FLAT} returns the union of all the regions which contribute to the \texttt{COVER} (more precisely, it	returns the contiguous region that starts from the first end and stops at the last end of the regions which would contribute to each region of the \texttt{COVER}).
		
		\item \texttt{SUMMIT} returns only those portions of the result regions of the \texttt{COVER} where the maximum number of regions intersect (more precisely, it returns regions that start from a position where
		the number of intersecting regions is not increasing afterwards and stops at a position where either the number of intersecting regions decreases, or it violates the max accumulation index).
		
		\item \texttt{HISTOGRAM} returns the nonoverlapping regions contributing to the cover, each with its accumulation index value, which is assigned to the \texttt{AccIndex} region attribute.
		
	\end{itemize}
	
	\item \texttt{JOIN} operation applies to two datasets, respectively called \textbf{anchor} (the first one) and \textbf{experiment} (the second one), and acts in two phases (each of them can be missing). In the first phase, pairs	of samples which satisfy the joinby predicate (also called meta-join predicate) are identified; in the second phase, regions that satisfy the \textbf{genometric predicate} are selected. The meta-join predicate allows selecting sample pairs with appropriate biological conditions (e.g., regarding the same cell line or antibody).
	
	\item \texttt{MAP} is a binary operation over two samples, respectively called \textbf{reference} and \textbf{experiment}. The operation is performed by first merging the samples in the reference operand, yielding to a single set of \textbf{reference regions}, and then by computing the aggregates over the values of the experiment regions that intersect with each reference region for each sample in the experiment operand. In other words, the experiment regions are mapped to the reference regions.
	
	
A \texttt{MAP} operation produces a regular structure, called \textbf{genometric space}, built as a matrix, where each experiment sample is associated with a column, each reference the region with a row and the matrix entries are typically scalars; such space can be inspected using heat maps, where rows and/or columns can be clustered to show patterns, or processed and evaluated through any matrix-based analytical process. In general, a \texttt{MAP} operation allows a quantitative reading of experiments with respect to reference regions; when the biological function of the reference regions is not known, the \texttt{MAP} helps in extracting the most interesting reference regions out of many candidates.

Fig. \ref{fig:map_example} shows the effect of this MAP operation on a small portion of the genome; the input consists of one reference sample with 3 regions and three mutation experiment samples,  the output consists of three samples, each with the same regions as the reference sample, whose features corresponds to the number of mutations which intersect with those regions.  The result can be interpreted as a (3×3) genome space.



\begin{figure}
	%\centering
	\includegraphics[width=1\textwidth]{map_example}
	\caption{Example of map using one sample as reference and three samples as experiment, using the Count aggregate function.}
	\label{fig:map_example}
\end{figure}
		
\end{itemize}


\subsection{Utility Operations}
\begin{itemize}
	
	\item \texttt{MATERIALIZE} operation saves the content of a dataset into the file system, and registers the saved dataset in the system to make it seamlessly usable in other GMQL queries. All	datasets defined in a GMQL query are, temporary by default; to see and preserve the content of	any dataset generated during a GMQL query, the dataset must be materialized. Any dataset can be materialized, however, the operation is time expensive. Therefore to achieve the best performance it is suggested to materialize the relevant data only \cite{Ceri2016, Kaitoua2016}.
	
	
\end{itemize}

\subsection{Biological Example}
This example uses the MAP operation to count the peak regions in each ENCODE ChIP-seq sample that intersect with a gene promoter (i.e., proximal regulatory region); then, in each sample it projects over (i.e., filters) the promoters with at least one intersecting peak, and counts these promoters. Finally, it extracts the top 3 samples with the highest number of such promoters.

\begin{center}
\begin{minipage}{10cm}
\begin{verbatim}

HM_TF = SELECT(dataType == ’ChipSeq’) ENCODE;
PROM = SELECT(annotation == ’promoter’) ANN;
PROM1 = MAP(peak_count AS COUNT) PROM HM_TF;
PROM2 = PROJECT(peak_count >= 1) PROM1;
PROM3 = AGGREGATE(prom_count AS COUNT) PROM2; 
RES = ORDER(DESC prom_count; TOP 3) PROM3;

\end{verbatim}

\end{minipage}
\end{center}

Further details about GMQL basic operators, GMQL syntax and relevant examples  of single statements and a notable combination of them are available at GMQL manual \footnote{GMQL Manual: \url{http://www.bioinformatics.deib.polimi.it/genomic_computing/GMQL/doc/GMQL_V2_manual.pdf}}  and GMQL user tutorial \footnote{GMQL User Tutorial: \url{http://www.bioinformatics.deib.polimi.it/genomic_computing/GMQL/doc/GMQLUserTutorial.pdf}}.



\subsection{Web Interface}

Web interfaces of GMQL system are designed and implemented by GeCo group in order to make the GMQL publicly available and easy to use by biologists and bioinformaticians. Two main services have been developed: a web service REST API and a web interface. Both of them are serving the same functionalities of browsing the datasets of genomic features and biological/clinical metadata that we collected in our system repository from ENCODE and TCGA, building GMQL queries upon them, and efficiently running such queries on thousands of samples in several heterogeneous datasets. Additionally, by using the user management system, private datasets can be uploaded and used in the same way as the ones available in the GMQL system. GMQL REST API is planned to be used by the external systems such as Galaxy \cite{giardine2005galaxy}, which is a scientific workflow and data integration system mainly used in the bioinformatics field, or any languages that can communicate to the REST services over HTTP\footnote{GMQL REST Services: \url{http://www.bioinformatics.deib.polimi.it/GMQL/interfaces/}}.
Figure \ref{fig:web_interface} illustrates the web user interface of GMQL.

\begin{figure} [H]
	%\centering
	\includegraphics[width=1\textwidth]{web_interface}
	\caption{The web graphical user interface of GMQL}
	\label{fig:web_interface}
\end{figure}
\subsection{Python Interface}

The Python interface, namely PyGMQL, can be considered as an alternative to the web interface. Figure \ref{fig:pygmql_interface} depicts the interaction between the user and the GMQL engine via PyGMQL. The Python library communicates GMQL through a Scala back-end. Besides, PyGMQL allows users to write GMQL queries in a syntax that meets the standard Python conventions. PyGMQL can be used both in local mode and the remote mode. The former performs the execution of the queries on the local machine, whereas the latter operates on the remote GMQL server. The users can also switch between local and remote modes during the course of the analysis pipeline. Furthermore, PyGMQL defines efficient data structures for the analysis of GDM data and it provides data analysis and machine learning packages tailored for the manipulation of genomic data. 

\begin{figure} [h]
	%\centering
	\includegraphics[width=1\textwidth]{pygmql_interface}
	\caption{High-level representation of the GMQL system}
	\label{fig:pygmql_interface}
\end{figure}

