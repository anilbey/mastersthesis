%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Third Chapter **********************************
%*******************************************************************************

\chapter{Analysis of Mutations in Cell-Specific Enhancers}
\label{chap:chp6}
% **************************** Define Graphics Path **************************
\ifpdf
    \graphicspath{{Chapter6/Figs/Raster/}{Chapter6/Figs/PDF/}{Chapter6/Figs/}}
\else
    \graphicspath{{Chapter6/Figs/Vector/}{Chapter6/Figs/}}
\fi

\section{Background}

Each cell of the human body contain the same DNA sequence regardless of the tissue they are belonging to. However, the cells that reside in different tissues show diverse functionality. This is due to the variation of the gene expression values. \citet{witzel2011scaffolds} visualize this phenomena on different tissues taken from the immune system, neural system and internal organs in Figure \ref{fig:tissue_var}.

%

\begin{figure} [h]
	\centering
	\includegraphics[width=1\textwidth]{tissue_var}
	\caption{Expression level variations on different tissues}
	\label{fig:tissue_var}
\end{figure}

An enhancer is a region of DNA that could be bound to proteins in order to enhance the activation of a particular gene \cite{blackwood1998going}. Figure \ref{fig:enhancer} depicts the activation process of a gene. Enhancers are usually positioned nearby the genes that they regulate. Yet, it could also happen that the enhancer is located far from the genes that it regulates. 

\begin{figure} [h]
	\centering
	\includegraphics[width=0.6\textwidth]{enhancer}
	\caption{Illustration of an enhancer activating a gene \cite{Enhancer1}}
	\label{fig:enhancer}
\end{figure}

It is known that mutations occurring in the coding region of a gene cause diseases. Moreover, it is proposed by the recent studies that the mutations happening on the enhancers could also be the cause of human diseases, \citet{emison2005common} and \citet{ramser2005unique}. To this extent, \citet{Pinoli2017} designed a GMQL pipeline to reveal the association between the traits and the mutations occurring on enhancers. This work is an extension of the work done by \citet{Pinoli2017} in the sense that it further analyzes the results retrieved by GMQL.

\section{Datasets}

The mutation data are retrieved from the GWAS Catalog \cite{gwas} which is provided by the European Bioinformatics Institute \cite{ebi} and the National Human Genome Research Institute \cite{nhgri}. The GWAS Catalog contains more than $49.769$ single nucleotide polymorphism-trait associations that are manually curated. GWAS is the abbreviation for \textit{genome-wide association studies} and it focuses on identifying the relationship between human diseases and the single nucleotide polymorphisms (SNP) across the entire genome. The GWAS studies compare the genome of the participants having a particular type of disease. The SNPs occurring more frequently in the participants having a specific disease are considered to be associated with that disease.

The enhancers, on the other hand, are retrieved from the ENCODE project. The main objective of the ENCODE project is to discover all of the functional elements of the human genome. This study particularly concentrates on the H3K4me3 histone modification. H3K4me3 stands for trimethylation of lysine 4 on histone H3 protein subunit and its corresponding nomenclature is given in Figure \ref{fig:h3k4me3} \cite{h3k4me3}. The modification of H3K4me3 is frequently associated with the active transcription of the surrounding genes \cite{guenther2007chromatin}. 


\begin{figure} [H]
	\centering
	\includegraphics[width=0.6\textwidth]{h3k4me3}
	\caption{Nomenclature for H3K4me3}
	\label{fig:h3k4me3}
\end{figure}


\section{Methodology}

The GMQL pipeline begins with the loading of the ENCODE data that comes in the narrow peak format. The peak values are assumed to be the exactly at the middle of the start and the end positions of the region. Later, the peak is extended 1500 units to the left and right in order to cover the mutations occurring nearby. This operation is followed by the merging of the samples having the same \texttt{biosample\_term\_name} attribute. Finally, the traits associated with the mutations are collected for every cell line.
The overall procedure is represented in Figure \ref{fig:pipeline}.

\begin{figure} [h]
	\centering
	\includegraphics[width=1\textwidth]{pipeline}
	\caption{Data analysis pipeline}
	\label{fig:pipeline}
\end{figure}


\begin{python}
import gmql as gl
# login credentials for the remote service
gl.login("username", "password")
# remote performing of the query
gl.set_mode("remote")

# the loading of the ENCODE data
enc = gl.load(name = "HG19_TCGA_dnaseq")
enc = enc[enc['target'] == 'H3K4me3-human']

# defining the position of the peak in the middle of the region
peaked = enc.reg_project(new_field_dict={'peak': enc.right/2 + enc.left/2})

# extending the peak 1500 units to the left and to the right 
large = peaked.reg_project(new_field_dict={'left': peaked.peak - 1500, 'right': peaked.peak + 1500})

# merging of the duplicates
rep = large.cover(minAcc=1, maxAcc="ANY", groupBy=['biosample_term_name'])

# to detect the enhancers specific to the cell lines
S = rep.cover(minAcc=1, maxAcc=2)
rep_count = rep.map(S)
cse = rep_count.reg_select(rep_count.count_REP_S > 0)

# loading of the mutations data
gwas = gl.load(name = "GWAS")

# detecting the mutations occurring in the ENCODE enhancers 
mapped = gwas.map(cse, new_reg_fields={'bag' = gl.BAG('trait')})
M = MAP( bag AS BAG( trait) ) CSE GWAS ;
N = M.reg_select(M.count_CSE_GWAS > 0).materialize()


\end{python}

After materializing the data, a GenoMetric space matrix is created to represent the mutation-trait matrix. The resulting matrix is of shape 51 rows × 1113 columns, i.e., 51 mutations and 111 traits. As expected, the resulting matrix is highly sparse (\%93 of the values are equal to zero). Given this very sparse matrix, we first identified the most frequently associated traits. Several mutations occurring in the cells including 'WERI-Rb-1', 'GM12875', 'fibroblast of lung', 'GM12864',
'LNCaP clone FGC', 'MCF-7', 'fibroblast of dermis', 'BE2C',
'B cell', 'cardiac mesoderm', 'HeLa-S3', 'fibroblast of gingiva',
'cardiac fibroblast' demonstrated stronger associations with certain diseases. However, many other mutations appeared to have very weak associations. The most frequent associations are reported in Appendix \ref{appendix:associations} for further biological examination.

\begin{comment}
Instead of the sentence on chapter 6, describe more in depth the technical content of the library and the fact that the library is public, integrated with the work on Luca on GMQL, and ready to be used by bio-informatics researchers/practitioners who like using python. 
\end{comment}
\section{Discussion and Conclusion}

To further investigate the data, we performed biclustering to cluster both the traits and the mutations together in an effort to detect the subset of mutations showing similar behavior on the subset of traits and vice versa. The conventional clustering algorithms would not be convenient to be employed at this stage since the most features of the samples are consisting of zeros, therefore the spectral biclustering method is applied. Obesity and obesity related traits indicated similar frequencies on the mutations in certain cells such as \texttt{GM12878} and \texttt{GM06990}, Figure \ref{fig:biclustering_res}.
This study was an attempt to demonstrate the power of PyGMQL on solving biological problems. The true underlying nature of histone modifications and their impact on certain traits is still an open research topic. We hope that the insights obtained from this work will support the further research and we look forward to seeing more usages of PyGMQL to solve open biological research problems. PyGMQL is publicly available on the Python Package Index \footnote{PyGMQL is available at \protect\url{https://pypi.python.org/pypi/gmql}} and it is ready to be used by the bioinformatics practitioners.  

\begin{figure} [h]
	\centering
	\includegraphics[width=0.7\textwidth]{biclustering_res}
	\caption{Biclustering the mutations and the traits together, rectangular shapes represent the similar frequencies of trait-mutation associations.}
	\label{fig:biclustering_res}
\end{figure}
